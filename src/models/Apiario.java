/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author familia
 */
public class Apiario {
    private String nome;
    private Integer numApiario;
    private Integer tamanho;
    private Integer qtdCaixas;
    private Integer qtdAbelhas;
     
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getNumApiario() {
        return numApiario;
    }

    public void setNumApiario(Integer numApiario) {
        this.numApiario = numApiario;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public Integer getQtdCaixas() {
        return qtdCaixas;
    }

    public void setQtdCaixas(Integer qtdCaixas) {
        this.qtdCaixas = qtdCaixas;
    }

    public Integer getQtdAbelhas() {
        return qtdAbelhas;
    }

    public void setQtdAbelhas(Integer qtdAbelhas) {
        this.qtdAbelhas = qtdAbelhas;
    }

    @Override
    public String toString() {
        return this.nome;
    }
    
    
}
