/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author familia
 */
public class Pasto {
    private String nome;
    private Integer qtdApiarios;
    private Integer distAgua;
    private Integer areaApicola;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdApiarios() {
        return qtdApiarios;
    }

    public void setQtdApiarios(Integer qtdApiarios) {
        this.qtdApiarios = qtdApiarios;
    }

    public Integer getDistAgua() {
        return distAgua;
    }

    public void setDistAgua(Integer distAgua) {
        this.distAgua = distAgua;
    }

    public Integer getAreaApicola() {
        return areaApicola;
    }

    public void setAreaApicola(Integer areaApicola) {
        this.areaApicola = areaApicola;
    }
    
}
