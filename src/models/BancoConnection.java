/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author jeferson
 */
public class BancoConnection {

    //Classe responsavel de abrir e fechar conexão do banco.
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    //ENDERECO:PORTA/NOME_BANCO
    private static final String URL = "jdbc:mysql://localhost:3306/projeto_integrador";

    //Usuario e senha do banco que você define quando cria o banco.
    private static final String USER = "root";
    private static final String PASS = "";

    //Abre conexão com o banco.
    public static Connection getConnection() {
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println("Erro: " + ex);
            return null;
        }
    }

    //Fecha conexão com o banco.
    public static void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Erro: " + ex);
            }
        }
    }

}