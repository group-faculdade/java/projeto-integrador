/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import models.BancoConnection;

/**
 *
 * @author familia
 */
public class ControleCaixa {
    Connection conexao = BancoConnection.getConnection();
    public void addCaixasAuto(Integer i, Integer foreign) throws SQLException{
        String str = "INSERT INTO caixa(nomeCaixa, idApiario) VALUES (?,?)";
        PreparedStatement ps = conexao.prepareStatement(str);
        for(int j=1; j<=i; j++){
            ps.setString(1, "Caixa "+j);
            ps.setInt(2, foreign);
            ps.execute();
        }      
    }
}
