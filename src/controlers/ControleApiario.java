/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import java.sql.*;
import models.BancoConnection;

/**
 *
 * @author familia
 */
public class ControleApiario {
    Connection conexao = BancoConnection.getConnection();
    public void addApiarioAuto(Integer i, Integer foreign) throws SQLException{
        String str = "INSERT INTO apiario(nomeApiario, idPasto) VALUES (?,?)";
        PreparedStatement ps = conexao.prepareStatement(str);
        for(int j=1; j<=i; j++){
            ps.setString(1, ("Apiario "+Integer.toString(j)));
            ps.setInt(2, foreign);
            ps.execute();
        }        
    }
    
    public void rmvApiarioAuto(Integer foreign) throws SQLException{
        Statement stmt = conexao.createStatement();
        String str = "DELETE FROM apiario WHERE idPasto=?;";
        PreparedStatement ps = conexao.prepareStatement(str);
        ps.setInt(1, foreign);
        ps.executeUpdate();       
    }
}
