/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Pasto;
import view.HomeJFrame;
import view.SplashScreen;

/**
 *
 * @author familia
 */
public class ProjetoIntegrador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {
            Logger.getLogger(ProjetoIntegrador.class.getName()).log(Level.SEVERE, null, ex);
        }
        HomeJFrame rodarProj = new HomeJFrame();
        rodarProj.setLocationRelativeTo(null);
        rodarProj.setVisible(true);
        rodarProj.setResizable(false);
    }    
}
