CREATE DATABASE projeto_integrador; 
use projeto_integrador;
SET GLOBAL lc_time_names=pt_BR;
CREATE TABLE pasto(
	idPasto INTEGER NOT NULL AUTO_INCREMENT,
    nomePasto VARCHAR (100) NOT NULL,
    disAgua INTEGER,
    areaApicola INTEGER,
    PRIMARY KEY(idPasto)
);

CREATE TABLE apiario(
	idApiario INTEGER NOT NULL AUTO_INCREMENT,
    nomeApiario VARCHAR(100) NOT NULL,
	idPasto INTEGER NOT NULL,
    PRIMARY KEY (idApiario),
    FOREIGN KEY (idPasto) REFERENCES pasto(idPasto)
);

CREATE TABLE caixa(
	idCaixa INTEGER NOT NULL AUTO_INCREMENT,
    nomeCaixa VARCHAR(100) NOT NULL,
    idApiario INTEGER NOT NULL,
    PRIMARY KEY(idCaixa),
    FOREIGN KEY (idApiario) REFERENCES apiario(idApiario)
);

CREATE TABLE quadro(
	idQuadro INTEGER NOT NULL AUTO_INCREMENT,
    nomeQuadro VARCHAR(100) NOT NULL,
    idCaixa INTEGER NOT NULL,
	PRIMARY KEY(idQuadro),
    FOREIGN KEY (idCaixa) REFERENCES caixa(idCaixa)
);

CREATE TABLE manejo(
	idManejo INTEGER NOT NULL AUTO_INCREMENT,
    idQuadro INTEGER NOT NULL,
    mel VARCHAR(3) NOT NULL,
    vazio VARCHAR(3) NOT NULL,
    polen VARCHAR(3) NOT NULL,
    ovo VARCHAR(3) NOT NULL,
    larva VARCHAR(3) NOT NULL,
    pupa VARCHAR(3) NOT NULL,
    escuro VARCHAR(3) NOT NULL,
    defeituoso VARCHAR(3) NOT NULL,
    semCera VARCHAR(3) NOT NULL,
    rainha VARCHAR(3) NOT NULL,
    dataManejo DATE NOT NULL,
    qtdAbelhas INTEGER NOT NULL,
    produMel INTEGER NOT NULL,
    PRIMARY KEY(idManejo),
    FOREIGN KEY(idQuadro) REFERENCES quadro(idQuadro)
);